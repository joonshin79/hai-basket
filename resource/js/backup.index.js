$(function() {
	$this = $('[data-wrap]');
	wrap = $this.data('wrap');
	var loding = "<div class=\"loading-container\">";
    loding+= "<div class=\"loading\"></div>"
    loding+= "<div id=\"loading-text\">loading</div>"
	loding+= "</div>";
	customeInfo = $('[data-module-header]').data('module-header'); //고객정보 배열로 가져옴

	// 장바구니 Start
	if(wrap == 'basket'){ // 장바구니에서만 동작
		var itemLi = $this.find('.basket-area>.item>ul>li');
		basketCode = $this.find('[data-basket-code]').data('basket-code'); //장바구니 코드 가져옴

		function itemUnitprice(){
			var itemUnit = 0;
			if(itemLi.length > 0){
				itemLi.each(function (index, item) {
					if($(this).find('input[type=checkbox]').is(":checked")){
						$(this).find('[data-quantity-price]').removeData('quantity-price');
						var quantityPrice = Number($(this).find('[data-quantity-price]').data('quantity-price'));
						itemUnit+= quantityPrice;
					}
				});
				var itemDelivery = Number($this.find('[data-item-delivery]').data('item-delivery'));
				if(itemUnit < 30000){
					var unitPrice = itemDelivery + itemUnit;
					$this.find('[data-item-delivery]').attr('data-item-delivery', 0).text('+ 배송비 2,500');
					$this.find('[data-total-delivery]').attr('data-total-delivery', totalPrice).text('+ ' + String(itemDelivery).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,') + ' 원');
				} else if(itemUnit > 30000){
					var unitPrice = itemUnit;
					$this.find('[data-item-delivery]').attr('data-item-delivery', 0).text('');
					$this.find('[data-total-delivery]').attr('data-total-delivery', 0).text(0 + ' 원');
				}
				var itemSale = Number($this.find('[data-item-sale]').data('item-sale'));
				var totalPrice = unitPrice - itemSale;
				$this.find('[data-item-unit]').attr('data-item-unit', itemUnit).text(String(itemUnit).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));
				$this.find('[data-unit-price]').attr('data-unit-price', unitPrice).text(String(unitPrice).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));
				$this.find('[data-total-price]').attr('data-total-price', totalPrice).text(String(totalPrice).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));
			} else{
				$this.find('[data-item-delivery]').attr('data-item-delivery', 0).text('');
				$this.find('[data-total-delivery]').attr('data-total-delivery', 0).text(0 + ' 원');
				$this.find('[data-item-unit]').attr('data-item-unit', 0).text('0');
				$this.find('[data-unit-price]').attr('data-unit-price', 0).text('0');
				$this.find('[data-total-price]').attr('data-total-price', 0).text('0');
			}
			return false;
		};

		$this.append(loding); //로딩바
		$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
		//장바구니 상품 불러오기
		$.ajax({
			type : "GET",
			dataType : "json",
			url : '../resource/json/cartitem.json',
			success:function(data){
				if(data.result == true){
					$.each(data.cartItem,function(i,item) {
						var ItemPriceReplace = item.Price.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
						var cartLi = "<li data-item-code='" + item.Code +"'>";
						cartLi+= "<button class='del'>삭제</button>";
						cartLi+= "<label><input type='checkbox' checked></label>";
						cartLi+= "<div class='item-thomb " + item.Mike + "'>";
						cartLi+= "<img src='" + item.Src +"' data-item-src='" + item.Src +"' alt=''>";
						cartLi+= "</div>";
						cartLi+= "<p class='item-name' data-item-name='" + item.Name +"'>" + item.Name +" <span data-item-quantity='" + item.Quantity +"'>" + item.Quantity +"</span>개</p>";
						cartLi+= "<p class='item-price'><strong data-item-price='" + item.Price +"' data-quantity-price='" + item.Price +"'>" + ItemPriceReplace +"</strong>원</p>";
						cartLi+= "<input class='quantity' type='number' value='" + item.Quantity +"'>";
						cartLi+= "<div class='item-message'>";
						cartLi+= "<p data-item-message=" + item.Message +">" + item.Message +"</p>";
						cartLi+= "<a href='#none'>더보기</a>";
						cartLi+= "</div>";
						cartLi+= "</li>";
						$this.find('.basket-area>.item>ul').append(cartLi);
					});
					itemLi = $this.find('.basket-area>.item>ul>li');
					itemUnitprice();
					$('.loading-container').remove(); //로딩바 삭제
					$('.wrap-mask').remove(); //마스크 삭제

					//상품삭제
					itemLi.find('.del').click(function(){
						$(this).parents('li').remove();
						itemLi = $this.find('.basket-area>.item>ul>li');
						itemUnitprice();		
					});

					//수량 변경
					itemLi.find('input[type=number]').on("propertychange change keyup paste input", function(){
						currentVal = Number($(this).val());
						var itemPrice = Number($(this).prev('.item-price').find('[data-item-price]').data('item-price'));
						var NewitemPrice = itemPrice * currentVal;
						$(this).parents('[data-item-code]').find('[data-quantity-price]').attr('data-quantity-price', NewitemPrice).text(String(NewitemPrice).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));
						$(this).parents('[data-item-code]').find('[data-item-quantity]').attr('data-item-quantity', currentVal).text(currentVal);
						itemUnitprice();
					});

					//추천상품 모달
					itemLi.find('>.item-message>a').click(function(e){
						e.preventDefault();
						var currentCode = $(this).parents('[data-item-code]').data('item-code');
						$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
						$this.append(loding); //로딩바
						var modal = $this.find('.modal.recommend');
						var itemCode = $(this).parents('[data-item-code]').data('item-code'); //itemCode 를 가져옴
						$.ajax({
							type : "GET",
							dataType : "json",
							url : '../resource/json/basketitem.json?itemCode=' + itemCode,
							success:function(data){
								if(data.result == true){
									modal.find('.swiper-container').remove();
									var swiper = "<div class='swiper-container'>";
									swiper+= "<ul class='swiper-wrapper'>";
									swiper+= "</ul>";
									swiper+= "</div>";
									modal.append(swiper);
									$.each(data.basketItem,function(i,item) {
										var price = item.Price.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
										var html = "<li class='swiper-slide' data-swiper-code='" + item.Code + "'>";
										html+= "<img src='" + item.Src + "' class='thomb' data-swiper-src='" + item.Src +"'>";
										html+= "<p class='item-name' data-swiper-name='" + item.Name +"'>" + item.Name + "</p>";
										html+= "<p class='item-price'><span data-swiper-price='" + item.Price + "'>" + price + "</span>원</p>";
										html+= "<button class='item-change'>교체</button>";
										html+= "</li>";
										modal.find('.swiper-wrapper').append(html);
									});
									modal.show().animate({bottom: "0"},300);
									$('.loading-container').remove(); //로딩바 삭제
									var swiper = new Swiper('.swiper-container', {
										slidesPerView: 'auto',
										spaceBetween: 5,
										slidesOffsetBefore: 10,
										slidesOffsetAfter: 10,
										pagination: {
											el: '.swiper-pagination',
											clickable: true,
										},
									});

									//추천상품 체인지 클릭
									modal.find('.item-change').click(function(){
										var _this = $(this).parents('[data-swiper-code]');
										var Code = _this.data('swiper-code');
										var Src = _this.find('[data-swiper-src]').data('swiper-src');
										var Name = _this.find('[data-swiper-name]').data('swiper-name');
										var Price = _this.find('[data-swiper-price]').data('swiper-price');
										var itemLi = $this.find('.basket-area>.item>ul>li');
										itemLi.each(function (index, item) {
											var changeCode = $(this).data('item-code');
											if(changeCode == currentCode){
												$(this).attr('data-item-code', Code);
												$(this).find('[data-item-name]').attr('data-item-name', Name).text(Name);
												$(this).find('[data-item-price]').attr({'data-item-price': Price, 'data-quantity-price': Price});
												$(this).find('[data-item-price]').text(String(Price).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,'));
												$(this).find('[data-item-src]').attr({"data-item-src": Src, "src": Src});
												$(this).find('input[type=number]').val('1');
											}
										});
										modal.animate({bottom: "-300px"},300);
										setTimeout(function() {
											modal.hide();
											$('.wrap-mask').remove();
										}, 300);
										itemLi = $this.find('.basket-area>.item>ul>li');
										itemUnitprice();
									});
								} else{
									alert('추천 상품이 없습니다.');
								}
							}
						});
						
						//추천상품 모달 닫기
						modal.find('>.header>button').click(function(){
							modal.animate({bottom: "-300px"},300);
							setTimeout(function() {
								modal.hide();
								$('.wrap-mask').remove(); //마스크 삭제
							}, 300);
						});
					});

				} else{
					alert('장바구니 상품이 없습니다.');
					$('.loading-container').remove(); //로딩바 삭제
					$('.wrap-mask').remove(); //마스크 삭제
				}
			}
		});

		//전체선택
		$this.find('.item-all input[type=checkbox]').click(function(){ 
			if($(this).prop("checked")) { 
				itemLi.find("input[type=checkbox]").prop("checked",true); 
			} else { 
				itemLi.find("input[type=checkbox]").prop("checked",false); 
			} 
		})

		//상품 체크 선택해제
		itemLi.find("input[type=checkbox]").change(function(){
			itemUnitprice();
		});

		//삭제버튼
		$this.find('.btn_area>.btn-del').click(function(){
			itemLi.each(function (index, item) {
				if($(this).find('input[type=checkbox]').is(":checked")){
					itemLi.eq(index).remove();
				}	
			});
			itemLi = $this.find('.basket-area>.item>ul>li');
			itemUnitprice();		
		});
		
		//주문하기
		$this.find('.btn_area>.btn-order').click(function(){
			var _this = $this.find('.total-area');
			var itemPrice = _this.find('[data-item-unit]').data('item-unit');
			var salePrice = _this.find('[data-item-sale]').data('item-sale');
			var delivery = _this.find('[data-total-delivery]').data('total-delivery');
			var totalPrice = _this.find('[data-total-price]').data('total-price');

			orderData = {
				"total":[
					{
						"itemPrice": itemPrice,
						"salePrice": salePrice,
						"delivery": delivery,
						"totalPrice": totalPrice
					},
				]
			}

			orderData.customeInfo = [];
			orderData.customeInfo.push(customeInfo);

			orderData.itemList = [];
			var itemLi = $this.find('.basket-area>.item>ul>li');
			itemLi.each(function (index, item) {
				if($(this).find('input[type=checkbox]').is(":checked")){
					var Code = $(this).data('item-code');
					var Name = $(this).find('[data-item-name]').data('item-name');
					var Price = $(this).find('[data-item-price]').data('item-price');
					var Quantity = $(this).find('input[type=number]').val();
					var Message = $(this).find('[data-item-message]').data('item-message');

					orderData.itemList.push({
						Code: Code,
						Name: Name,
						Price: Price,
						Quantity: Quantity,
						Message: Message
					});
				}
			});

			//주문내용 orderData
			console.log(orderData);
		});

	};
	// 장바구니 End

	// 주문하기 Start
	if(wrap == 'checkout'){ // 주문하기에서만 동작
		checkoutCode = $this.find('[data-checkout-code]').data('checkout-code'); //주문하기 코드 가져옴
		$this.find('[data-order-code] button').click(function(){
			$(this).parents('[data-order-code]').next('ul').is(':visible');
			if($this.find('[data-order-code]').next('ul').is(':visible')){
				$this.find('[data-order-code]').next('ul').slideUp(300);
			} else{
				$this.find('[data-order-code]').next('ul').slideDown(300);
			}
		});
	}
	// 주문하기 End

	// main Start
	if(wrap == 'main'){ // main에서만 동작
		var swiper = new Swiper('.main-swiper', {
			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			}
		});
		var swiper = new Swiper('.recommend-swiper', {
			slidesPerView: 'auto',
			freeMode: true,
			spaceBetween: 5,
			slidesOffsetBefore: 15,
			slidesOffsetAfter: 15
		});
		$this.find('.main-swiper .swiper-pagination').append('<span class=more>전체보기</span>');
	}
	// main End

	// 카테고리 Start
	if(wrap == 'category'){ // 카테고리에서만 동작
	}
	// 카테고리 End

});
