$(function() {
	$this = $('[data-wrap]');
	wrap = $this.data('wrap');
	var loding = "<div class=\"loading-container\">";
    loding+= "<div class=\"loading\"></div>"
    loding+= "<div id=\"loading-text\">loading</div>"
	loding+= "</div>";



	// 장바구니 Start
	if(wrap == 'basket'){ // 장바구니에서만 동작
		var itemLi = $this.find('.basket-area>.item>ul>li');
		var modal = $this.find('.modal.recommend');

		//추천상품 모달
		itemLi.find('>.item-message>a').click(function(e){
			$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
			modal.show().animate({bottom: "0"},300);
			var swiper = new Swiper('.swiper-container', {
				slidesPerView: 'auto',
				spaceBetween: 5,
				slidesOffsetBefore: 10,
				slidesOffsetAfter: 10,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
			});
		});

		//추천상품 모달 닫기
		modal.find('>.header>button').click(function(){
			modal.animate({bottom: "-300px"},300);
			setTimeout(function() {
				modal.hide();
				$('.wrap-mask').remove(); //마스크 삭제
			}, 300);
		});

	}
	// 장바구니 End

	// 주문하기 Start
	if(wrap == 'checkout'){ // 주문하기에서만 동작
		checkoutCode = $this.find('[data-checkout-code]').data('checkout-code'); //주문하기 코드 가져옴
		$this.find('[data-order-code] button').click(function(){
			if($this.find('[data-order-code]').next('ul').is(':visible')){
				$this.find('[data-order-code]').next('ul').slideUp(300);
			} else{
				$this.find('[data-order-code]').next('ul').slideDown(300);
			}
		});
	}
	// 주문하기 End

	// main Start
	if(wrap == 'main'){ // main에서만 동작
		var swiper = new Swiper('.main-swiper', {
			pagination: {
				el: '.swiper-pagination',
				type: 'fraction',
			}
		});
		var swiper = new Swiper('.recommend-swiper', {
			slidesPerView: 'auto',
			freeMode: true,
			spaceBetween: 5,
			slidesOffsetBefore: 15,
			slidesOffsetAfter: 15
		});
		$this.find('.main-swiper .swiper-pagination').append('<span class=more>전체보기</span>');

		var modal = $this.find('.add-cart');
		var itemLi = $this.find('.item-list>li');
		//장바구니 모달 열기
		itemLi.find('.item-thomb>i.off').click(function(e){
			$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
			modal.show().animate({bottom: "0"},210);
			var swiper = new Swiper('.swiper-container', {
				slidesPerView: 'auto',
				spaceBetween: 5,
				slidesOffsetBefore: 10,
				slidesOffsetAfter: 10,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
			});
		});

		//장바구니 모달 닫기
		modal.find('>button').click(function(){
			modal.animate({bottom: "-210px"},300);
			setTimeout(function() {
				modal.hide();
				$('.wrap-mask').remove(); //마스크 삭제
			}, 300);
		});

	}
	// main End

	// 카테고리 Start
	if(wrap == 'category'){ // 카테고리에서만 동작
		var modal = $this.find('.add-cart');
		var itemLi = $this.find('.item-list>li');
		//장바구니 모달 열기
		itemLi.find('.item-thomb>i').click(function(e){
			$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
			modal.show().animate({bottom: "0"},210);
			var swiper = new Swiper('.swiper-container', {
				slidesPerView: 'auto',
				spaceBetween: 5,
				slidesOffsetBefore: 10,
				slidesOffsetAfter: 10,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
			});
		});

		//장바구니 모달 닫기
		modal.find('>button').click(function(){
			modal.animate({bottom: "-210px"},300);
			setTimeout(function() {
				modal.hide();
				$('.wrap-mask').remove(); //마스크 삭제
			}, 300);
		});
	}
	// 카테고리 End

	// 상품상세 Start
	if(wrap == 'detail'){ // 상품상세에서만 동작
		var modal = $this.find('.add-cart');
		//장바구니 모달 열기
		$this.find('.btn-order').click(function(e){
			$this.append('<div class=wrap-mask></div>'); //딤 마스크 추가
			modal.show().animate({bottom: "0"},210);
			var swiper = new Swiper('.swiper-container', {
				slidesPerView: 'auto',
				spaceBetween: 5,
				slidesOffsetBefore: 10,
				slidesOffsetAfter: 10,
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
			});
		});

		//장바구니 모달 닫기
		modal.find('>button').click(function(){
			modal.animate({bottom: "-210px"},300);
			setTimeout(function() {
				modal.hide();
				$('.wrap-mask').remove(); //마스크 삭제
			}, 300);
		});
	}
	// 상품상세 End
});
